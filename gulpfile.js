var gulp = require('gulp'); // npm install gulp --save-dev
var less = require('gulp-less'); // npm install gulp-less --save-dev
var spritesmith = require('gulp.spritesmith'); // npm install gulp.spritesmith
var browserSync = require('browser-sync'); // npm install browser-sync --save-dev
var useref = require('gulp-useref'); // npm install gulp-useref --save-dev
var uglify = require('gulp-uglify'); // npm install gulp-uglify --save-dev
var gulpIf = require('gulp-if'); // npm install gulp-if --save-dev
var minifyCSS = require('gulp-minify-css'); // npm install gulp-minify-css
var imagemin = require('gulp-imagemin'); // npm install gulp-imagemin --save-dev
var cache = require('gulp-cache'); // npm install gulp-cache --save-dev
var del = require('del'); // npm install del --save-dev
var runSequence = require('run-sequence'); // npm install run-sequence --save-dev
var path = require('path'); 

// Start browserSync server

gulp.task('browserSync', function() {
	browserSync({
		server: {
			baseDir: 'app'
		}
	})
});

gulp.task('less', function(){
  return gulp.src('app/less/**/*.less')
      .pipe(less({
          paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
      .pipe(gulp.dest('app/css'))
      .pipe(browserSync.reload({
        stream: true
      }))
});

// Sprites generation

gulp.task('sprite', function () {
  var spriteData = gulp.src('app/images/icons/*.png').pipe(spritesmith({
    imgName: 'sprite.png',
    cssName: 'sprite.less',
    padding: 4
  }));
  spriteData.img.pipe(gulp.dest('app/images/sprites/'));
  spriteData.css.pipe(gulp.dest('app/less/'));
});


// Watchers
gulp.task('watch', function() {
  	gulp.watch('app/less/**/*.less', ['less']);
  	gulp.watch('app/*.html', browserSync.reload);
  	gulp.watch('app/js/**/*.js', browserSync.reload);
})


// Optimizing CSS and JavaScript

gulp.task('useref', function(){
  // var assets = useref.assets();

  	return gulp.src('app/*.html')
    	// .pipe(assets)
    	.pipe(gulpIf('*.css', minifyCSS()))
    	.pipe(gulpIf('*.js', uglify()))
    	// .pipe(assets.restore())
    	.pipe(useref())
    	.pipe(gulp.dest('dist'))
});


// Optimizing Images 

gulp.task('images', function(){
  	return gulp.src('app/images/**/*.+(png|jpg|jpeg|gif|svg)')
  		.pipe(cache(imagemin({
      		interlaced: true
    	})))
  		.pipe(gulp.dest('dist/images'))
});


// Copying fonts 

gulp.task('fonts', function() {
    return gulp.src('app/fonts/**/*')
      .pipe(gulp.dest('dist/fonts'))
});


// Cleaning 

gulp.task('clean', function(callback) {
  	return del.sync('dist').then(function(cb) {
    	return cache.clearAll(cb);
  	});
});

gulp.task('clean:dist', function(callback){
  	return del.sync(['dist/**/*', '!dist/images', '!dist/images/**/*']);
});

// Build Sequences
// ---------------

gulp.task('default', function (callback) {
  	runSequence(['less', 'browserSync', 'watch'], callback)
});

gulp.task('build', function (callback) {
  	runSequence('less', 'useref', 'images', 'fonts', callback)
});