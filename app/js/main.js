$(document).ready(function(){
	var $fieldTag = $('.form-control--tags');
	$fieldTag.attr('size', $fieldTag.attr('placeholder').length + 4);
	$('.row-tags').on('click', $fieldTag, function(){
		$fieldTag.parent().addClass('row-tags--options');
	});
});